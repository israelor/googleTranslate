const stripe = require('stripe')('sk_test_JShgQg3HQaX35BRvGLzyNU15');
const express = require('express');
const router = express.Router();

const asyncHandler = fn => (req, res, next) =>
  Promise
    .resolve(fn(req, res, next))
    .catch(next);

router.post('/createCustomer', asyncHandler(async(req, res, next) => {
  const sourceID = req.body.sourceId;

  var customer = await stripe.customers.create(
    { 
      source: sourceID
    });
    res.send(customer);
  }) 
);

router.post('/updateCustomer', asyncHandler(async(req, res, next) => {
  const cusId = req.body.customerId;
  const sourceId = req.body.sourceId;

  const newSource = await stripe.customers.createSource(cusId, {
    source: sourceId
  });

  res.send(newSource);
})
);

router.post('/updateDefaultSource', asyncHandler(async(req, res, next) => {
  const cusId = req.body.customerId;
  const sourceId = req.body.sourceId;

  const newDefault = await stripe.customers.update(cusId, {
    default_source: sourceId
  });

  res.send(newDefault);
})
);

router.delete('/:cusId/:sourceId', asyncHandler(async(req, res, next) => {

  const customerId = req.params.cusId;
  const sourceId = req.params.sourceId;

  const deleted = await stripe.customers.deleteSource(
    customerId,
    sourceId
  );

  res.send(deleted);
})
);

router.get('/:id', asyncHandler(async(req, res, next) => {
  const customerId = req.params.id;

  var customer = await stripe.customers.retrieve(customerId);
  res.send(customer);
}));

module.exports = router;
