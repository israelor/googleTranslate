const express = require('express');
const router = express.Router();
const translate = require('google-translate-api');

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('this is a translate page');
});

router.post('/', function (req, res, next) {
  translate(req.body.msg, { from: req.body.from, to: req.body.to }).then(text => {
    let msgObj = {
     };
    msgObj[req.body.from] = req.body.msg;
    msgObj[req.body.to] = text.text;
    res.send(msgObj);
    //=> nl
  }).catch(err => {
    res.sendStatus(500).send("cannot translate");
  });
});

module.exports = router;
